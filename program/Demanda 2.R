library(maptools)
library(maps)
library(rgdal)
library(RColorBrewer)
library(leaflet)
require("rgeos")
require("rgdal")
library(data.table)
library(dplyr)
library(plyr)
library(raster)
library(readxl)



#### LENDO A SHAPE DE BACIAS
bacia<-readShapeSpatial("data\\SNIRH_RegioesHidrograficas\\SNIRH_RegioesHidrograficas.shp",verbose=F,force_ring = T)

## LENDO A SHAPE DE MUNICIOPIOS
mun<-readShapeSpatial("data\\Shapes - Brasil por Municipios\\municipios_2010.shp",verbose=F,force_ring = T)

## PLOTANDO AS BACIAS NO LEAFLET
# leaflet(bacia) %>% addTiles() %>%
#   addPolylines(color = "lawngreen", weight = 1, smoothFactor = 0.5,
#                opacity = 1.0, fillOpacity = 0.5) %>%
#   addPolylines(data=mun,color = "red", weight = 1, smoothFactor = 0.5,
#               opacity = 1.0, fillOpacity = 0.5)

base2 <- read_excel(path = "data/RGP 03_2017 - Qtd de Pescadores por Municipio.xlsx")


base2$nome<-base2$MUNICÍPIO
mun@data<-full_join(base2,mun@data, by="nome")


## CRUZANDO AS SHAPES
# Results<-gIntersects(bacia,mun,byid=TRUE)
# colnames(Results)<-bacia@data$RHI_NM
# rownames(Results)<-mun@data$nome

Results2<-gIntersection(mun,bacia,byid=TRUE)

cruzado<-NULL
for(i in 1:length(Results2@polygons)){
  cruzado_temp<-data.table(id=Results2@polygons[[i]]@ID, area=Results2@polygons[[i]]@area)
  cruzado<-rbind(cruzado,cruzado_temp)
}

cruzado$linha_mun<-gsub("(.*)\\s.*","\\1",cruzado$id)
cruzado$linha_rh<-gsub(".*\\s(.*)","\\1",cruzado$id)

cruzado$linha_rh<-as.numeric(cruzado$linha_rh)+1
cruzado$linha_mun<-as.numeric(cruzado$linha_mun)+1

base.bacia<-bacia@data
base.mun<-mun@data

base.bacia$linha_rh<-1:length(base.bacia$RHI_SG)
base.mun$linha_mun<-1:length(base.mun$id)

setnames(base.mun,"id","id.mun")

cruzado<-left_join(cruzado,base.bacia, by="linha_rh")
cruzado<-left_join(cruzado,base.mun, by="linha_mun")

malha.cruzada<-Results2
row.names(cruzado) <- row.names(malha.cruzada)

malha.cruzada <- SpatialPolygonsDataFrame(malha.cruzada, cruzado)
malha.cruzada@data
saveRDS(cruzado,file="C:\\Users\\b18756824\\Documents\\4. Bacias Hidrograficas - Leonardo\\cruza_shape_bacia\\cruzado.rds")

library(maptools)
writeSpatialShape(malha.cruzada, "C:\\Users\\b18756824\\Documents\\4. Bacias Hidrograficas - Leonardo\\cruza_shape_bacia\\malha_cruzada")

