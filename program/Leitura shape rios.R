
library(maptools)
library(maps)
library(rgdal)
library(RColorBrewer)
library(leaflet)
require("rgeos")
require("rgdal")
library(geosphere)

#### LENDO A SHAPE
rios<-readShapeSpatial(file.choose(),verbose=F,force_ring = T)

## ARQUIVO QUE CONTEM INFORMACOES DOS RIOS
rios@data

## ARQUIVO QUE CONTEM OS ARQUIVOS COM COORDENADAS DOS RIOS
rios@lines

## ARQUIVO COM LATITUDES E LONGITUDES DOS RIOS
rios@lines[[1]]@Lines[[1]]

## LENDO SHAPE DO BRASIL
brasil<-readShapeSpatial(file.choose(),verbose=F,force_ring = T)

brasil@polygons

## PLOTANDO OS RIOS PARA TESTE
plot(rios@lines[[1]]@Lines[[1]][1],rios@lines[[1]]@Lines[[1]][2], pch=18, col="red")

## PLOTANDO UM RIO PARA AMOSTRA NO LEAFLET
leaflet(rios@lines[[1]]@Lines[[1]]) %>% addTiles() %>%
  addPolylines(color = "lawngreen", weight = 10, smoothFactor = 0.5,
              opacity = 1.0, fillOpacity = 0.5)

## DEFININDO ESSE RIO COMO OBJETO
rio1<-rios@lines[[1]]@Lines[[1]]

## CRIANDO O BUFFER SOBRE O RIO
tmp2 = Lines( list(Line(dados)), ID="0")
lne = SpatialLines(list(tmp2)) # This is the river 'line'
lne_buffer=gBuffer(lne,width=0.002) # This is the buffered river

## ESTUDANDO A CODIFICACAO DO BUFFER
lne@proj4string@projargs<-"+proj=longlat +datum=WGS84"
lne2<-lne
rgdal::CRS(lne2)<-"+proj=longlat +datum=WGS84"
testebuf2<-spTransform(lne,CRS( "+proj=longlat +datum=WGS84" ))

## PLOTANDO VIA LEAFLET O BUFFER
testebuf<-gBuffer(lne, width=1)
leaflet(lne) %>% addTiles() %>%
  addPolylines(color = "lawngreen", weight = 3,
               opacity = 1, fillOpacity = 1) 




dados=NULL
for(j in 1:length(rios@lines)){
  for(i in 1:length(rios@lines[[j]]@Lines)){
    dados<-rbind.data.frame(dados,rios@lines[[j]]@Lines[[i]]@coords)
  }
}
dados=NULL
for(j in 1:length(rios@lines)){
  for(i in 1:length(rios@lines[[j]]@Lines)){
    dados<-rbind.data.frame(dados,rios@lines[[j]]@Lines[[i]]@coords)
    print(i);print(j)
  }
}

dados=NULL
for(j in 1:1){
  for(i in 1:length(rios@lines[[j]]@Lines)){
    dados<-rbind.data.frame(dados,rios@lines[[j]]@Lines[[i]]@coords)
    print(i);print(j)
  }
}


leaflet() %>%addTiles()%>%
  for(j in 1:length(rios@lines)-1){
    for(i in 1:length(rios@lines[[j]]@Lines)){
      addPolylines(lng = rios@lines[[j]]@Lines[[i]]@coords[,1],
                   lat = rios@lines[[j]]@Lines[[i]]@coords[,2],
                   color = "lawngreen", weight = 3, opacity = 1, fillOpacity = 1) %>% }}
     addPolylines(lng = rios@lines[[54267]]@Lines[[1]]@coords[,1],
             lat = rios@lines[[54267]]@Lines[[1]]@coords[,2],
             color = "lawngreen", weight = 3,
             opacity = 1, fillOpacity = 1)




leaflet() %>% addTiles() %>%
  addPolylines(lng = rios@lines[[1]]@Lines[[1]]@coords[,1],
               lat = rios@lines[[1]]@Lines[[1]]@coords[,2],
               color = "lawngreen", weight = 3,
               opacity = 1, fillOpacity = 1) %>%
  addPolylines(lng = rios@lines[[1]]@Lines[[2]]@coords[,1],
               lat = rios@lines[[1]]@Lines[[2]]@coords[,2],
               color = "lawngreen", weight = 3,
               opacity = 1, fillOpacity = 1)
