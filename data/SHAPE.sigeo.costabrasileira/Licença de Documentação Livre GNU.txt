Licen�a de Documenta��o Livre GNU
Vers�o 1.1, Mar�o de 2000

Copyright (C) 2000  Free Software Foundation, Inc.
59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
� permitido a qualquer um copiar e distribuir c�pias exatas
deste documento de licen�a, mas n�o � permitido alter�-lo.

0. INTRODU��O

O prop�sito desta Licen�a � deixar um manual, livro-texto ou outro documento escrito 
``livre'' no sentido de liberdade: assegurar a qualquer um a efetiva liberdade de copiar ou 
redistribu�-lo, com ou sem modifica��es, comercialmente ou n�o. Secundariamente, esta 
Licen�a mant�m para o autor e editor uma forma de ter cr�dito por seu trabalho, sem ser 
considerado respons�vel pelas modifica��es feitas por terceiros.
Esta licen�a � um tipo de ``copyleft'' (``direitos revertidos''), o que significa que deriva��es 
do documento precisam ser livres no mesmo sentido. Ela complementa a GNU Licen�a 
P�blica Geral (GNU GPL), que � um copyleft para software livre.
N�s fizemos esta Licen�a para que seja usada em manuais de software livre, porque 
software livre precisa de documenta��o livre: um programa livre deve ser acompanhado de 
manuais que forne�am as mesmas liberdades que o software possui. Mas esta Licen�a n�o 
est� restrita a manuais de software; ela pode ser usada para qualquer trabalho em texto, 
independentemente do assunto ou se ele � publicado como um livro impresso. N�s 
recomendamos esta Licen�a principalmente para trabalhos cujo prop�sito seja de intru��o 
ou refer�ncia.

1. APLICABILIDADE E DEFINI��ES

Esta Licen�a se aplica a qualquer manual ou outro texto que contenha uma nota colocada 
pelo detentor dos direitos autorias dizendo que ele pode ser distribu�do sob os termos desta 
Licen�a. O ``Documento'', abaixo, se refere a qualquer tal manual ou texto. Qualquer 
pessoa do p�blico � um licensiado e � referida como ``voc�''.
Uma ``Vers�o Modificada'' do Documento se refere a qualquer trabalho contendo o 
documento ou uma parte dele, quer copiada exatamente, quer com modifica��es e/ou 
traduzida em outra lingua.
Uma ``Se��o Secund�ria'' � um ap�ndice ou uma se��o inicial do Documento que trata 
exclusivamente da rela��o dos editores ou dos autores do Documento com o assunto geral 
do Documento (ou assuntos relacionados) e n�o cont�m nada que poderia ser incluido 
diretamente nesse assunto geral. (Por exemplo, se o Documento � em parte um livro texto 
de matem�tica, a Se��o Secund�ria pode n�o explicar nada de matem�tica). Essa rela��o 
poderia ser uma quest�o de liga��o hist�rica com o assunto, ou mat�rias relacionadas, ou de 
posi��es legais, comerciais, filos�ficas, �ticas ou pol�ticas relacionadas ao mesmo.
As ``Se��es Invariantes'' s�o certas Se��es Secund�rias cujos t�tulos s�o designados, como 
sendo de Se��es Invariantes, na nota que diz que o Documento � publicado sob esta 
Licen�a.
Os ``Textos de Capa'' s�o certos trechos curtos de texto que s�o listados, como Textos de 
Capa Frontal ou Textos da Quarta Capa, na nota que diz que o texto � publicado sob esta 
Licen�a.
Uma c�pia ``Transparente'' do Documento significa uma c�pia que pode ser lida 
automaticamente, representada num formato cuja especifica��o esteja dispon�vel ao p�blico 
geral, cujos conte�dos possam ser vistos e editados diretamente e sem mecanismos 
especiais com editores de texto gen�ricos ou (para imagens compostas de pixels) programas 
de pintura gen�ricos ou (para desenhos) por algum editor de desenhos grandemente 
difundido, e que seja pass�vel de servir como entrada a formatadores de texto ou para 
tradu��o autom�tica para uma variedade de formatos que sirvam de entrada para 
formartadores de texto. Uma c�pia feita em um formato de arquivo outrossim Transparente 
cuja constitui��o tenha sido projetada para atrapalhar ou desencorajar modifica��es 
subsequentes pelos leitores n�o � Transparente. Uma c�pia que n�o � ``Transparente'' � 
chamada de ``Opaca''.
Exemplos de formatos que podem ser usados para c�pias Transparentes incluem ASCII 
simples sem marca��es, formato de entrada do Texinfo, formato de entrada do LaTeX, 
SGML ou XML usando uma DTD disponibilizada publicamente, e HTML simples, 
compat�vel com os padr�es, e projetado para ser modificado por pessoas. Formatos opacos 
incluem PostScript, PDF, formatos propriet�rios que podem ser lidos e editados apenas 
com processadores de texto propriet�rios, SGML ou XML para os quais a DTD e/ou 
ferramentas de processamento e edi��o n�o estejam dispon�veis para o p�blico, e HTML 
gerado automaticamente por alguns editores de texto com finalidade apenas de sa�da.
A ``P�gina do T�tulo'' significa, para um livro impresso, a p�gina do t�tulo propriamente 
dita, mais quaisquer p�ginas subseq�entes quantas forem necess�rias para conter, de forma 
leg�vel, o material que esta Licen�a requer que apare�a na p�gina do t�tulo. Para trabalhos 
que n�o tenham uma tal p�gina do t�tulo, ``P�gina do T�tulo'' significa o texto pr�ximo da 
apari��o mais proeminente do t�tulo do trabalho, precedendo o in�cio do corpo do texto.

2. FAZENDO C�PIAS EXATAS

Voc� pode copiar e distribuir o Documento em qualquer meio, de forma comercial ou n�o 
comercial, desde que esta Licen�a, as notas de copyright, e a nota de licen�a dizendo que 
esta Licen�a se aplica ao documento estejam reproduzidas em todas as c�pias, e que voc� 
n�o acrescente nenhuma outra condi��o quaisquer que sejam �s desta Licen�a.
Voc� n�o pode usar medidas t�cnicas para obstruir ou controlar a leitura ou confec��o de 
c�pias subsequentes das c�pias que voc� fizer ou distribuir. Entretanto, voc� pode aceitar 
compensa��o em troca de c�pias. Se voc� distribuir uma quantidade grande o suficiente de 
c�pias, voc� tamb�m precisa respeitar as condi��es da se��o 3.
Voc� tamb�m pode emprestar c�pias, sob as mesmas condi��es colocadas acima, e voc� 
tamb�m pode exibir co�pias publicamente.

3. FAZENDO C�PIAS EM QUANTIDADE

Se voc� p�blicar c�pias do Documento em n�mero maior que 100, e a nota de licen�a do 
Documento obrigar Textos de Capa, voc� precisa incluir as c�pias em capas que tragam, 
clara e legivelmente, todos esses Textos de Capa: Textos de Capa da Frente na capa da 
frente, e Textos da Quarta Capa na capa de tr�s. Ambas as capas tamb�m precisam 
identificar clara e legivelmente voc� como o editor dessas c�pias. A capa da frente precisa 
apresentar o t�tulo completo com todas as palavras do t�tulo igualmente proeminentes e 
vis�veis. Voc� pode adicionar outros materiais �s capas. Fazer c�pias com modifica��es 
limitadas �s capas, tanto quanto estas preservem o t�tulo do documento e satisfa�am essas 
condi��es, pode tratado como c�pia exata em outros aspectos.
Se os textos requeridos em qualquer das capas for muito volumoso para caber de forma 
leg�vel, voc� deve colocar os primeiros (tantos quantos couberem de forma razo�vel) na 
capa verdadeira, e continuar os outros nas p�ginas adjacentes.
Se voc� publicar ou distribuir c�pias Opacas do Documento em n�mero maior que 100, 
voc� precisa ou incluir uma c�pia Transparente que possa ser lida automaticamente com 
cada c�pia Opaca, ou informar em ou com cada c�pia Opaca a localiza��o de uma c�pia 
Transparente completa do Documento acess�vel publicamente em uma rede de 
computadores, � qual o p�blico usu�rio de redes tenha acesso a download gratuito e 
an�nimo utilizando padr�es p�blicos de protocolos de rede. Se voc� utilizar o segundo 
m�todo, voc� precisa tomar cuidados razoavelmente prudentes, quando iniciar a 
distribui��o de c�pias Opacas em quantidade, para assegurar que esta c�pia Transparente 
vai permanecer acess�vel desta forma na localiza��o especificada por pelo menos um ano 
depois da �ltima vez em que voc� distribuir uma c�pia Opaca (diretamente ou atrav�s de 
seus agentes ou distribuidores) daquela edi��o para o p�blico.
� pedido, mas n�o � obrigat�rio, que voc� contate os autores do Documento bem antes de 
redistribuir qualquer grande n�mero de c�pias, para lhes dar uma oportunidade de prover 
voc� com uma vers�o atualizada do Documento.

4. MODIFICA��ES

Voc� pode copiar e distribuir uma Vers�o Modificada do Documento sob as condi��es das 
se��es 2 e 3 acima, desde que voc� publique a Vers�o Modificada estritamente sob esta 
Licen�a, com a Vers�o Modificada tomando o papel do Documento, de forma a licenciar a 
distribui��o e modifica��o da Vers�o Modificada para quem quer que possua uma c�pia da 
mesma. Al�m disso, voc� precisa fazer o seguinte na vers�o modificada:
*	A. Usar na P�gina de T�tulo (e nas capas, se alguma) um t�tulo distinto daquele do 
Documento, e daqueles de vers�es anteriores (que deveriam, se houvesse algum, 
estarem listados na se��o Hist�rico do Documento). Voc� pode usar o mesmo t�tulo 
de uma vers�o anterior se o editor original daquela vers�o lhe der permiss�o.
*	B. Listar na P�gina de T�tulo, como autores, uma ou mais das pessoas ou entidades 
respons�veis pela autoria das modifica��es na Vers�o Modificada, conjuntamente 
com pelo menos cinco dos autores principais do Documento (todos os seus autores 
principais, se ele tiver menos que cinco).
*	C. Colocar na P�gina de T�tulo o nome do editor da Vers�o Modificada, como o 
editor.
*	D. Preservar todas as notas de copyright do Documento.
*	E. Adicionar uma nota de copyright apropriada para suas pr�prias modifica��es 
adjacente �s outras notas de copyright.
*	F. Incluir, imediatamente depois das notas de copyright, uma nota de licen�a dando 
ao p�blico o direito de usar a Vers�o Modificada sob os termos desta Licen�a, na 
forma mostrada no Adendo abaixo.
*	G. Preservar nessa nota de licen�a as listas completas das Se��es Invariantes e os 
Textos de Capa requeridos dados na nota de licen�a do Documento.
*	H. Incluir uma c�pia inalterada desta Licen�a.
*	I. Preservar a se��o entitulada ``Hist�rico'', e seu t�tulo, e adicionar � mesma um 
item dizendo pelo menos o t�tulo, ano, novos autores e editor da Vers�o Modificada 
como dados na P�gina de T�tulo. Se n�o houver uma sess�o denominada 
``Hist�rico''; no Documento, criar uma dizendo o t�tulo, ano, autores, e editor do 
Documento como dados em sua P�gina de T�tulo, ent�o adicionar um item 
descrevendo a Vers�o Modificada, tal como descrito na senten�a anterior.
*	J. Preservar o endere�o de rede, se algum, dado no Documento para acesso p�blico 
a uma c�pia Transparente do Documento, e da mesma forma, as localiza��es de 
rede dadas no Documento para as vers�es anteriores em que ele foi baseado. Elas 
podem ser colocadas na se��o ``Hist�rico''. Voc� pode omitir uma localiza��o na 
rede para um trabalho que tenha sido publicado pelo menos quatro anos antes do 
Documento, ou se o editor original da vers�o a que ela ser refira der sua permiss�o.
*	K. Em qualquer se��o entitulada ``Agradecimentos''; ou ``Dedicat�rias'';, preservar 
o t�tulo da se��om e preservar a se��o em toda subst�ncia e tim de cada um dos 
agradecimentos de contribuidores e/ou dedicat�rias dados.
*	L. Preservar todas as Se��es Invariantes do Documento, inalteradas em seus textos 
ou em seus t�tulos. N�meros de se��o ou equivalentes n�o s�o considerados parte 
dos t�tulos da se��o.
*	M. Apagar qualquer se��o entitulada ``Endossos'';. Tal sess�o n�o pode ser inclu�da 
na Vers�o Modificada.
*	N. N�o re-entitular qualquer se��o existente com o t�tulo ``Endossos''; ou com 
qualquer outro t�tulo dado a uma Se��o Invariante.
Se a Vers�o Modificada incluir novas se��es iniciais ou ap�ndices que se qualifiquem 
como Se��es Secund�rias e n�o contenham nenhum material copiado do Documento, voc� 
pode optar por designar alguma ou todas aquelas se��es como invariantes. Para fazer isso, 
adicione seus t�tulos � lista de Se��es Invariantes na nota de licen�a da Vers�o Modificada. 
Esses t�tulos precisam ser diferentes de qualquer outro t�tulo de se��o.
Voc� pode adicionar uma se��o entitulada ``Endossos'';, desde que ela n�o contenha 
qualquer coisa al�m de endossos da sua Vers�o Modificada por v�rias pessoas ou entidades 
- por exemplo, declara��es de revisores ou de que o texto foi aprovado por uma 
organiza��o como a defini��o oficial de um padr�o.
Voc� pode adicionar uma passagem de at� cinco palavras como um Texto de Capa da 
Frente , e uma passagem de at� 25 palavras como um Texto de Quarta Capa, ao final da 
lista de Textos de Capa na Vers�o Modificada. Somente uma passagem de Texto da Capa 
da Frente e uma de Texto da Quarta Capa podem ser adicionados por (ou por acordos feitos 
por) qualquer entidade. Se o Documento j� incluir um texto de capa para a mesma capa, 
adicionado previamente por voc� ou por acordo feito com alguma entidade para a qual voc� 
esteja agindo, voc� n�o pode adicionar um outro; mas voc� pode trocar o antigo, com 
permiss�o expl�cita do editor anterior que adicionou a passagem antiga.
O(s) autor(es) e editor(es) do Documento n�o d�o permiss�o por esta Licen�a para que seus 
nomes sejam usados para publicidade ou para assegurar ou implicar endossamento de 
qualquer Vers�o Modificada.

5. COMBINANDO DOCUMENTOS

Voc� pode combinar o Documento com outros documentos publicados sob esta Licen�a, 
sob os termos definidos na se��o 4 acima para vers�es modificadas, desde que voc� inclua 
na combina��o todas as Se��es Invariantes de todos os documentos originais, sem 
modifica��es, e liste todas elas como Se��es Invariantes de seu trabalho combinado em sua 
nota de licen�a.
O trabalho combinado precisa conter apenas uma c�pia desta Licen�a, e Se��es Invariantes 
Id�nticas com m�ltiplas ocorr�ncias podem ser substitu�das por apenas uma c�pia.Se 
houver m�ltiplas Se��es Invariantes com o mesmo nome mas com conte�dos distintos, fa�a 
o t�tulo de cada se��o �nico adicionando ao final do mesmo, em par�nteses, o nome do 
autor ou editor origianl daquela se��o, se for conhecido, ou um n�mero que seja �nico. 
Fa�a o mesmo ajuste nos t�tulos de se��o na lista de Se��es Invariantes nota de licen�a do 
trabalho combinado.
Na combina��o, voc� precisa combinar quaisquer se��es intituladas ``Hist�rico''; dos 
diversos documentos originais, formando uma se��o entitulada ``Hist�rico''; da mesma 
forma combine quaisquer se��es entituladas ``Agradecimentos'', ou ``Dedicat�rias''. Voc� 
precisa apagar todas as se��es intituladas como ``Endosso''.

6. COLET�NEAS DE DOCUMENTOS

Voc� pode fazer uma colet�nea consitindo do Documento e outros documentos publicados 
sob esta Licen�a, e substituir as c�pias individuais desta Licen�a nos v�rios documentos 
com uma �nica c�pia inclu�da na colet�nea, desde que voc� siga as regras desta Licen�a 
para c�pia exata de cada um dos Documentos em todos os outros aspectos.
Voc� pode extrair um �nico documento de tal colet�nea, e distribu�-lo individualmente sob 
esta Licen�a, desde que voc� insira uma c�pia desta Licen�a no documento extra�do, e siga 
esta Licen�a em todos os outros aspectos relacionados � c�pia exata daquele documento.

7. AGREGA��O COM TRABALHOS INDEPENDENTES

Uma compila��o do Documento ou derivados dele com outros trabalhos ou documentos 
separados e independentes, em um volume ou m�dia de distribui��o, n�o conta como uma 
Vers�o Modificada do Documento, desde que n�o seja reclamado nenhum copyright de 
compila��o seja reclamado pela compila��o. Tal compila��o � chamada um ``agregado'', e 
esta Licen�a n�o se aplica aos outros trabalhos auto-contidos compilados junto com o 
Documento, s� por conta de terem sido assim compilados, e eles n�o s�o trabalhos 
derivados do Documento.
Se o requerido para o Texto de Capa na se��o 3 for aplic�vel a essas c�pias do Documento, 
ent�o, se o Documento constituir menos de um quarto de todo o agregado, os Textos de 
Capa do Documento podem ser colocados em capas adjacentes ao Documento dentro do 
agregado. Sen�o eles precisam aparecer nas capas de todo o agregado.

8. TRADU��O

A tradu��o � considerada como um tipo de modifica��o, ent�o voc� pode distribuir 
tradu��es do Documento sob os termos da se��o 4. A substitui��o de Se��es Invariantes 
por tradu��es requer uma permiss�o especial dos detentores do copyright das mesmas, mas 
voc� pode incluir tradu��es de algumas ou de todas as Se��es Invariantes em adi��o as 
vers�es orginais dessas Se��es Invariantes. Voc� pode incluir uma tradu��o desta Licen�a 
desde que voc� tamb�m inclua a vers�o original em Ingl�s desta Licen�a. No caso de 
discord�ncia entre a tradu��o e a vers�o original em Ingl�s desta Licen�a, a vers�o original 
em Ingl�s prevalecer�.

9. T�RMINO

Voc� n�o pode copiar, modificar, sublicenciar, ou distribuir o Documento exceto como 
expressamente especificado sob esta Licen�a. Qualquer outra tentativa de copiar, modificar, 
sublicenciar, ou distribuir o Documento � nula, e resultar� automaticamente no t�rmino de 
seus direitos sob esta Licen�a. Entretanto, terceiros que tenham recebido c�pias, ou direitos, 
de voc� sob esta Licen�a n�o ter�o suas licen�as terminadas tanto quanto esses terceiros 
permane�am em total acordo com esta Licen�a.

10. REVIS�ES FUTURAS DESTA LICEN�A

A Free Software Foundation pode publicar novas vers�es revisadas da Licen�a de 
Documenta��o Livre GNU de tempos em tempos. Tais novas vers�es ser�o similares em 
esp�rito � vers�o presente, mas podem diferir em detalhes ao abordarem novos porblemas e 
preocupa��es. Veja http://www.gnu.org/copyleft/.
A cada vers�o da Licen�a � dado um n�mero de vers�o distinto. Se o Documento 
especificar que uma vers�o particular desta Licen�a ``ou qualquer vers�o posterior'' se 
aplica ao mesmo, voc� tem a op��o de seguir os termos e condi��es daquela vers�o 
espec�fica, ou de qualquer vers�o posterior que tenha sido publicada (n�o como rascunho) 
pela Free Software Foundation. Se o Documento n�o especificar um n�mero de Vers�o 
desta Licen�a, voc� pode escolher qualquer vers�o j� publicada (n�o como rascunho) pela 
Free Software Foundation.

